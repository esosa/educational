import numpy as np
import bag_of_words

def train(vocabulary, categories, corpus):
    num = np.array([[0]*len(vocabulary) for k in range(len(categories))])
    den = np.array([[0] for k in range(len(categories))])
    for k, text in enumerate(corpus):
        print('{}/{}'.format(k+1, len(corpus)))
        for cat in categories:
            if text[0] == cat:
                bow = np.array(bag_of_words.bag_of_words(vocabulary, text[1]))
                num[categories.index(cat)] += bow
                den[categories.index(cat)] += sum(bow)
    print(num)
    return num/den

def predict(m, pc, vocabulary, text):
    cs = []
    for c, p in enumerate(pc):
        S = np.log(pc[p]) + np.dot(m[c], np.array(bag_of_words.bag_of_words(vocabulary, text)) > 0)
        cs.append(S)
    return cs.index(max(cs))
