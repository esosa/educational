import numpy as np
import sys
import sqlite3
import bag_of_words
import nb 
import random

conn = sqlite3.connect(sys.argv[1], isolation_level=None, check_same_thread=False)
cursor = conn.cursor()

results = []
categories = []
corpus = []
for cat in sys.argv[2:-1]:
    cat = int(cat)
    cursor.execute('''SELECT content, category_id
                    FROM stories
                    INNER JOIN stories_content ON story_content_id = stories_content.rowid
                    INNER JOIN categories ON category_id = categories.id
                    WHERE category_id = ?
                    LIMIT ?''', (cat, sys.argv[-1]))
    result = cursor.fetchall()
    results.append(result)
    categories.append(int(cat))
    corpus += tuple((cat, row[0]) for row in result)



vocabulary = bag_of_words.vocabulary(corpus)
pc = {categories[k]: len([x for x in corpus if x[0] == categories[k]])/len(corpus) for k in range(len(categories))}
m = nb.train(vocabulary, categories, corpus)
print(m)
random.shuffle(corpus)


sql_query = '''SELECT content, category_id
                FROM stories
                INNER JOIN stories_content ON story_content_id = stories_content.rowid
                INNER JOIN categories ON category_id = categories.id
                 AND stories.id IN (SELECT stories.id FROM stories WHERE {} ORDER BY RANDOM() LIMIT ?)'''.format(' OR '.join(['category_id = '+str(x) for x in sys.argv[2:-1]]))
print(sql_query)
cursor.execute(sql_query, (sys.argv[-1],))
results = cursor.fetchall()
corpus = tuple((row[1], row[0]) for row in results)

predictions = []
actuals = []

for text in corpus:
    prediction = categories[nb.predict(m, pc, vocabulary, text[1])]
    actual = text[0]
    predictions.append(prediction)
    actuals.append(actual)
    print(prediction, actual)

print(np.mean(np.array(predictions) == np.array(actuals)))
