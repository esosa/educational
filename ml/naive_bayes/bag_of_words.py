import re

def clean_word(word):
    word = re.sub('^\W+', '', word)
    word = re.sub('\W+$', '', word)
    word = word.casefold()
    return word

def vocabulary(corpus):
    vocabulary = set()
    corpus = [x[1] for x in corpus]
    for text in corpus:
        for word in text.split():
            vocabulary.add(clean_word(word))
    return list(vocabulary)

def bag_of_words(vocabulary, text):
    bag_of_words = [0]*len(vocabulary)
    for word in text.split():
        word = clean_word(word)
        try:
            bag_of_words[vocabulary.index(word)] += 1
        except Exception as e:
            pass
    return bag_of_words
