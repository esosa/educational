#!/usr/bin/python
import sys
from scanner import Scanner

class Lox():
    had_error = False

    def run_file(self, path):
        f = open(path)
        self.run(f.read())
        if (self.had_error):
            raise SystemExit(65)

    def run_prompt(self):
        while True:
            self.run(input('> '))

    def run(self, source):
        scanner = Scanner(source)
        tokens = scanner.scan_tokens()
        for token in tokens:
            print(token)

    def error(self, line, message):
        self.report(line, '', message)

    def report(self, line, where, message):
        print('[line {}] Error {}: {}'.format(line, where, message))
        self.had_error = True

    def main(self):
        if len(sys.argv) > 2:
            print('Usage: plox [script]')
        elif len(sys.argv) == 2:
            self.run_file(sys.argv[1])
        else:
            self.run_prompt()

if __name__ == '__main__':
    Lox().main()
