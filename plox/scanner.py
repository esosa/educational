from token import Token
from token_type import *

class Scanner():
    def __init__(self, source):
        self._tokens = []
        self._source = source
        self._start = 0
        self._current = 0
        self._line = 1

    def scan_tokens(self):
        while not self._is_at_end():
            # We are at the beginning of the next lexeme.
            self._start = self._current
            self._scan_token()

        self._tokens.append(Token(EOF, '', None, self._line))
        return self._tokens

    def _scan_token(self):
        c = self._advance()
        if c == '(':   self._add_token(LEFT_PAREN)
        elif c == ')': self._add_token(RIGHT_PAREN)
        elif c == '{': self._add_token(LEFT_BRACE)
        elif c == '}': self._add_token(RIGHT_BRACE)
        elif c == ',': self._add_token(COMMA)
        elif c == '.': self._add_token(DOT)
        elif c == '-': self._add_token(MINUS)
        elif c == '+': self._add_token(PLUS)
        elif c == ';': self._add_token(SEMICOLON)
        elif c == '*': self._add_token(STAR)
        elif c == '!': self._add_token(BANG_EQUAL if self._match('=') else BANG)
        elif c == '=': self._add_token(EQUAL_EQUAL if self._match('=') else EQUAL)
        elif c == '<': self._add_token(LESS_EQUAL if self._match('=') else LESS)
        elif c == '>': self._add_token(GREATER_EQUAL if self._match('=') else GREATER)
        elif c in (' ', '\r', '\t'): pass
        elif c == '\n':
            self._line += 1
        elif c == '/':
            if self._match('/'):
                while self._peek() != '\n' and not self._is_at_end():
                    self._advance()
            else:
                self._add_token(SLASH)
        elif c == '"': self._string()
        else:
            if self._is_digit(c):
                self._number()
            else:
                from lox import Lox
                Lox().error(self._line, 'Unexpected character')

    def _number(self):
        while self._is_digit(self._peek()):
            self._advance()

        if self._peek() == '.' and self._is_digit(self._peek_next()):
            self._advance()

            while self._is_digit(self._peek()):
                self._advance()

        self._add_token(NUMBER, float(self._source[self._start:self._current]))
        
    def _is_digit(self, c):
        return c in [str(_) for _ in range(10)]

    def _string(self):
        while self._peek() != '"' and not self._is_at_end():
            if self._peek == '\n':
                line += 1
            self._advance()

        if (self._is_at_end()):
            Lox().error(line, 'Unterminated string.')
            return

        self._advance()
        value = self._source[self._start+1:self._current-1]
        self._add_token(STRING, value)
        
    def _advance(self):
        self._current += 1
        return self._source[self._current-1]

    def _peek(self):
        if self._is_at_end():
            return '\0'
        else:
            return self._source[self._current]

    def _peek_next(self):
        if self._current+1 >= len(self._source):
            return '\0'
        return self._source[self._current+1]

    def _match(self, expected):
        if self._is_at_end():
            return False
        if self._source[self._current] != expected:
            return False
        self._current += 1
        return True

    def _is_at_end(self):
        return self._current >= len(self._source)

    def _add_token(self, type, literal=None):
        text = self._source[self._start:self._current]
        self._tokens.append(Token(type, text, literal, self._line))
