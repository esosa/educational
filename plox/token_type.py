from enum import Enum

TokenType = Enum('TokenType',
                 ' '.join(
                  # Single-character tokens.
                 ['LEFT_PAREN', 'RIGHT_PAREN', 'LEFT_BRACE', 'RIGHT_BRACE', 
                  'COMMA', 'DOT', 'MINUS', 'PLUS', 'SEMICOLON', 'SLASH',
                  'STAR',
                  # One or two character tokens.
                  'BANG', 'BANG_EQUAL',
                  'EQUAL', 'EQUAL_EQUAL',
                  'GREATER', 'GREATER_EQUAL',
                  'LESS', 'LESS_EQUAL',
                  # Literals.
                  'IDENTIFIER', 'STRING', 'NUMBER',
                  # Keywords.
                  'AND', 'CLASS', 'ELSE', 'FALSE', 'FUN', 'FOR', 'IF', 'NIL',
                  'OR', 'PRINT', 'RETURN', 'SUPER', 'THIS', 'TRUE', 'VAR',
                  'WHILE',
                  'EOF']))

globals().update(TokenType.__members__) # https://stackoverflow.com/questions/28130683/how-does-one-get-an-enums-members-into-the-global-namespace
