import edu.princeton.cs.algs4.*;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedList<Item> implements Iterable<Item> {
  public Node first, last;

  private class Node {
    Item item;
    Node next;
  }

  public void add(Item x) {
    Node node = new Node();
    node.item = x;
    if (last == null) {
      first = last = node;
    } else {
      last.next = node;
      last = node;
    }
  }

  public String toString() {
    String s = "[";
    for (Item k : this) {
      s += k + ", ";
    }
    return s + "]";
  }

  public String firstItem() {
    if (this.isEmpty()) throw new NoSuchElementException();
    return "" + first.item;
  }

  public String lastItem() {
    if (this.isEmpty()) throw new NoSuchElementException();
    return "" + last.item;
  }

  public Iterator<Item> iterator() {
    return new ListIterator();
  }

  public void delete(int n) {
    boolean deleted = false;
    int k = 0;
    for (Node current = first, previous = null; deleted == false && current != null; previous = current, current = current.next, k++) {
      if (k == n) {
        StdOut.println(k);
        if (previous == null) { // it could be that the node is the first one in the list, the last one in the list or one in between
          first = current.next;
        }
        if (current.next == null) {
          last = previous;
          if (previous != null) previous.next = null;
        }
        if (!(current.next == null || previous == null)) {
          previous.next = current.next;
        }
        deleted = true;
      }
    }
    if (deleted == false) throw new NoSuchElementException();
  }

  public void revert() {
    if (first != null && first.next != null) {
      last = first;
      for (Node previous = first, current = previous.next, next = current.next; current != null; current = next) {
        if (previous == first) {
          first.next = null;
          last = previous;
        }
        next = current.next;
        current.next = previous;
        previous = current;
        if (next == null) {
          first = current;
        }
      }
    }
  }

  private void removeAfter(Node node) {
    if (node != null && node.next != null) {
      node.next = (node.next).next;
    }
  }

  public void testRemoveAfter(int n) {
    int k = 0;
    for (Node current = first; current != null; current = current.next, k++) {
      if (n == k) {
        this.removeAfter(current);
      }
    }
  }

  public static boolean findString(LinkedList<String> list, String x) {
    for (String y : list) {
      if (x.equals(y)) return true;
    }
    return false;
  }

  public boolean isEmpty() {
    if (first == null) {
      assert last != null: "Reference to first node is null but the reference to the last node isn't null.";
      return true;
    } else {
      return false;
    }
  }

  private class ListIterator implements Iterator<Item> {
    private Node current = first;

    public boolean hasNext() {
      return current != null;
    }
    public void remove() { }

    public Item next() {
      Item item = current.item;
      current = current.next;
      return item;
    }
  }

  public static void main(String[] args) {
    LinkedList<Integer> list = new LinkedList<Integer>();
    while(!StdIn.isEmpty()) {
      list.add(StdIn.readInt());
    }
    int k = 0;
    StdOut.println(list);
    list.testRemoveAfter(4);
    StdOut.println(list);
  }
}
