/* API
 * public class Steque<Item> implements Iterable<Item>
 *
 *
 *              Steque        create an empty Steque
 *  boolean     isEmpty()     is the steque empty?
 *  int         size()        number of elements in the steque
 *  void        push()        add an item to the beginning of the steque
 *  void        pop()         remove an item from the beginning of the steque
 *  void        enqueue()     add an item to the end of the steque
 *
 */
import edu.princeton.cs.algs4.*;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class Steque<Item> implements Iterable<Item> {
  private Node first, last;
  private int N = 0; // number of elements

  private class Node {
    Item item;
    Node next;
  }

  public Iterator<Item> iterator() {
    return new StequeIterator();
  }

  private class StequeIterator implements Iterator<Item> {
    private Node current = first;

    public boolean hasNext() {
      return current != null;
    }
    public void remove() { }

    public Item next() {
      Item item = current.item;
      current = current.next;
      return item;
    }
  }

  public boolean isEmpty() {
    if (first == null) {
      assert last != null: "Reference to first node is null but the reference to the last node isn't null.";
      return true;
    } else {
      return false;
    }
  }
  public int size() {
    return N;
  }

  public void push(Item x) {
    N++;
    Node node = new Node();
    node.item = x;
    if (first == null) {
      first = last = node;
    } else {
      node.next = first;
      first = node;
    }
  }

  public void enqueue(Item x) {
    N++;
    Node node = new Node();
    node.item = x;
    if (first == null) {
      first = last = node;
    } else {
      last.next = node;
      last = node;
    }
  }

  public Item pop() {
    N--;
    Item item;
    item = first.item;
    first = first.next;
    return item;
  }

  public String toString() {
    String s = "[";
    for (Item k : this) {
      s += k + ", ";
    }
    return s + "]";
  }

  public static void main(String[] args) {
    Steque<String> steque = new Steque<String>();
    String s;
    String[] pair = new String[2];
    while(!StdIn.isEmpty()) {
      s = StdIn.readLine();
      pair = s.split(" ");
      StdOut.printf("%s %s\n", pair[0], pair[1]);
      if (pair[0].equals("push")) {
        steque.push(pair[1]);
      } else if (pair[0].equals("enqueue")) {
        steque.enqueue(pair[1]);
      }
      StdOut.println(steque);
      StdOut.printf("size of steque: %d\n", steque.size());
    }
    StdOut.println(steque);
    while (steque.size() > 0) {
      steque.pop();
      StdOut.println(steque);
      StdOut.printf("size of steque: %d\n", steque.size());
    }
  }
}
