import edu.princeton.cs.algs4.*;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class Josephus {

  public static void main(String[] args) {
    int N, M;
    N = Integer.parseInt(args[0]);
    M = Integer.parseInt(args[1]);
    Queue<Integer> queue = new Queue<Integer>();
    for (int k = 1; k < N; k++) {
      queue.enqueue(k);
    }
    queue.enqueue(0);
    while (!queue.isEmpty()) {
      StdOut.printf("%d ", queue.dequeue());
      for (int k = 0 ; k < M-1 && !queue.isEmpty(); k++) {
        queue.enqueue(queue.dequeue());
      }
    }
    StdOut.println();
  }
}
