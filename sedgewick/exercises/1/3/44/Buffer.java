import edu.princeton.cs.algs4.*;

public class Buffer{
    char cursor;
    Stack<Character> left;
    Stack<Character> right;

    public Buffer() {
      left = new Stack<Character>();
      right = new Stack<Character>();
    }
    
    public void insert(char c) {
      cursor = c;
    }

    public char delete() {
      char oldCursor = cursor;
      cursor = '\u0000';
      return oldCursor;
    }

    public void right(int k) {
      if (right.isEmpty()) {
        if (k == 1) {
          left.push(cursor);
          cursor = '\u0000';
        } else {
          return;
        }
      } else {
        for (int i = 0; i < k && !right.isEmpty(); i++) {
          left.push(cursor);
          cursor = right.pop();
        }
      }
    }

    public void left(int k) {
      for (int i = 0; i < k-1 && !left.isEmpty(); i++) {
        right.push(cursor);
        cursor = left.pop();
      }
    }

    public String toString() {
        Stack<Character> reverse = new Stack<Character>();
        for (Character c : left) {
          reverse.push(c);
        }
        for (Character c : reverse) {
          StdOut.printf("%c ", c);
          left.push(c);
        }
        StdOut.printf("%c_", cursor);
        StdOut.println(right);
        
//        StdOut.printf("%s %s %s", left, ">" + cursor + "<", right);
        return "";
    }

    public static void main(String[] args) {
      Buffer buffer = new Buffer();
      String s = "lakra";
      {
        char c = 0;
        for (int k = 0; k < s.length(); k++) {
          buffer.insert(s.charAt(k));
          buffer.right(1);
          buffer.toString();
        }
        buffer.left(1);
        buffer.left(1);
        buffer.toString();
      }
      StdOut.println(buffer.left);
      StdOut.println(buffer.cursor);
      StdOut.println(buffer.right);
    }


}

