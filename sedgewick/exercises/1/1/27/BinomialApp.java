public class BinomialApp {
  public static void printTable(double[][] table) {
    for (int i = 0; i < 10; i++) {
      for (int j = 0; j < 10; j++) {
        System.out.printf("%f ", table[i][j]);
      }
      System.out.println("");
    }
  }

  public static double binomial(int N, int k, double p, double[][] table)
  {
    if (N == 0 && k == 0) {
      table[N][k] = 1.0;
      return 1.0;
    }
    if (N < 0 || k < 0) {
      return 0.0;
    }
    if (table[N][k] == -1.0) {
      table[N][k] = (1.0 - p) *binomial(N-1, k, p, table) + p*binomial(N-1, k-1, p, table);
      return table[N][k];
    } else {
      return table[N][k];
    }
  }

  public static double binomial0(int N, int k, double p) {
    if ((N == 0) && (k == 0)) return 1.0;
    if ((N < 0) || (k < 0)) return 0.0;
    return (1 - p)*binomial0(N-1, k, p) + p*binomial0(N-1, k-1, p);
  }

  public static void main(String[] args) {
    int N = Integer.parseInt(args[0]);
    int k = Integer.parseInt(args[1]);
    double p = Double.parseDouble(args[2]);
    double[][] table = new double[N+1][N+1];
    for (int i = 0; i < N+1; i++) {
      for (int j = 0; j < N+1; j++) {
        table[i][j] = -1.0;
      }
    }
    

    System.out.println(N + " " + k + " " + p);
    System.out.println(binomial(N, k, p, table));
    System.out.println(binomial0(N, k, p));
  }
}
