import edu.princeton.cs.algs4.*;

public class RandomConnections{
    public static void main(String[] args) {
      int N;
      double t1, t2, t, p;
      N = StdIn.readInt();
      p = StdIn.readDouble();
      StdDraw.setXscale(-2, 2);
      StdDraw.setYscale(-2, 2);

      StdDraw.circle(0, 0, 1);
      StdDraw.setPenRadius(0.01);
      StdDraw.setPenColor(StdDraw.BLUE);
      for (int k = 0; k < N; k++) {
        t = k*2.0*Math.PI/N;
        StdDraw.point(Math.cos(t), Math.sin(t));
      }
      StdDraw.setPenRadius(0.005);
      StdDraw.setPenColor(StdDraw.GRAY);
      for (int i = 0; i < N; i++) {
        t1 = i*2.0*Math.PI/N;
        for (int j = 0; j < N; j++) {
          t2 = j*2.0*Math.PI/N;
          if (StdRandom.bernoulli(p) == true) {
            StdDraw.line(Math.cos(t1), Math.sin(t1), Math.cos(t2), Math.sin(t2));
          }
        }
      }
    }
}

